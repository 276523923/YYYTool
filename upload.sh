#!/bin/bash
confirmed="n"

gitTagAdd() {
git stash
git pull origin master --tags
git stash pop

VersionString=`grep -E 's.version.*=' YYYTool.podspec`
VersionNumber=`tr -cd 0-9 <<<"$VersionString"`

VersionString=${VersionString##*=}
VersionNumber=`tr -d "'" <<<"$VersionString"`
echo "current version is ${VersionNumber}"

git add .
git commit -am ${VersionNumber}
git tag ${VersionNumber}
git push origin master --tags
}
echo -e "================================================\n"
echo -e "tag version 上传"
gitTagAdd
echo -e "================================================\n"

pod repo push YYYPrivatePods YYYTool.podspec --allow-warnings --use-libraries
