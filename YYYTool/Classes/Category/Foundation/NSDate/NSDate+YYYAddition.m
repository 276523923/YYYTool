//
//  NSDate+Addition.m
//  Pods
//
//  Created by 叶越悦 on 2017/7/25.
//
//

#import "NSDate+YYYAddition.h"

NS_INLINE NSCalendar *YYY_Calendar_Gregorian() {
    static NSCalendar *yyy_calendar_gregorian = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        yyy_calendar_gregorian = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    });
    return yyy_calendar_gregorian;
}

@implementation NSDate (YYYProperty)

- (NSInteger)year {
    return [[YYY_Calendar_Gregorian() components:NSCalendarUnitYear fromDate:self] year];
}

- (NSInteger)month {
    return [[YYY_Calendar_Gregorian() components:NSCalendarUnitMonth fromDate:self] month];
}

- (NSInteger)day {
    return [[YYY_Calendar_Gregorian() components:NSCalendarUnitDay fromDate:self] day];
}

- (NSInteger)hour {
    return [[YYY_Calendar_Gregorian() components:NSCalendarUnitHour fromDate:self] hour];
}

- (NSInteger)minute {
    return [[YYY_Calendar_Gregorian() components:NSCalendarUnitMinute fromDate:self] minute];
}

- (NSInteger)second {
    return [[YYY_Calendar_Gregorian() components:NSCalendarUnitSecond fromDate:self] second];
}

- (NSInteger)nanosecond {
    return [[YYY_Calendar_Gregorian() components:NSCalendarUnitSecond fromDate:self] nanosecond];
}

- (NSInteger)weekday {
    return [[YYY_Calendar_Gregorian() components:NSCalendarUnitWeekday fromDate:self] weekday];
}

- (NSInteger)weekdayOrdinal {
    return [[YYY_Calendar_Gregorian() components:NSCalendarUnitWeekdayOrdinal fromDate:self] weekdayOrdinal];
}

- (NSInteger)weekOfMonth {
    return [[YYY_Calendar_Gregorian() components:NSCalendarUnitWeekOfMonth fromDate:self] weekOfMonth];
}

- (NSInteger)weekOfYear {
    return [[YYY_Calendar_Gregorian() components:NSCalendarUnitWeekOfYear fromDate:self] weekOfYear];
}

- (NSInteger)yearForWeekOfYear {
    return [[YYY_Calendar_Gregorian() components:NSCalendarUnitYearForWeekOfYear fromDate:self] yearForWeekOfYear];
}

- (NSInteger)quarter {
    return [[YYY_Calendar_Gregorian() components:NSCalendarUnitQuarter fromDate:self] quarter];
}

- (BOOL)isLeapMonth {
    return [[YYY_Calendar_Gregorian() components:NSCalendarUnitQuarter fromDate:self] isLeapMonth];
}

- (BOOL)isLeapYear {
    NSUInteger year = [[YYY_Calendar_Gregorian() components:NSCalendarUnitYear fromDate:self] year];
    return ((year % 400 == 0) || ((year % 100 != 0) && (year % 4 == 0)));
}

- (BOOL)isToday {
    if (fabs(self.timeIntervalSinceNow) >= 60 * 60 * 24) {
        return NO;
    }
    return [NSDate date].day == self.day;
}

@end

NS_INLINE NSString *YYY_NSDateFormatterString(NSString *dateString) {
    typedef NSString *(^YYYNSDateFormatterParseBlock)(NSString *string);
    static YYYNSDateFormatterParseBlock formatterParseBlocks[35] = {0};
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        {
            /*
             2014-01-20
             */
            formatterParseBlocks[10] = ^(NSString *string) {
                return @"yyyy-MM-dd";
            };
        }
        {
            /*
             2014-01-20 12:24:48
             2014-01-20T12:24:48   // Google
             2014-01-20 12:24:48.000
             2014-01-20T12:24:48.000
             */
            formatterParseBlocks[19] = ^(NSString *string) {
                if ([string characterAtIndex:10] == 'T') {
                    return @"yyyy-MM-dd'T'HH:mm:ss";
                } else {
                    return @"yyyy-MM-dd HH:mm:ss";
                }
            };
            
            formatterParseBlocks[23] = ^(NSString *string) {
                if ([string characterAtIndex:10] == 'T') {
                    return @"yyyy-MM-dd'T'HH:mm:ss.SSS";
                } else {
                    return @"yyyy-MM-dd HH:mm:ss.SSS";
                }
            };
        }
        {
            /*
             2014-01-20T12:24:48Z        // Github, Apple
             2014-01-20T12:24:48+0800    // Facebook
             2014-01-20T12:24:48+12:00   // Google
             2014-01-20T12:24:48.000Z
             2014-01-20T12:24:48.000+0800
             2014-01-20T12:24:48.000+12:00
             */
            formatterParseBlocks[20] = ^(NSString *string) {
                return @"yyyy-MM-dd'T'HH:mm:ssZ";
            };
            formatterParseBlocks[24] = ^(NSString *string) {
                if ([string hasSuffix:@"Z"]) {
                    return @"yyyy-MM-dd'T'HH:mm:ss.SSSZ";
                } else {
                    return @"yyyy-MM-dd'T'HH:mm:ssZ";
                }
            };
            formatterParseBlocks[25] = ^(NSString *string) {
                return @"yyyy-MM-dd'T'HH:mm:ssZ";
            };
            formatterParseBlocks[28] = ^(NSString *string) {
                return @"yyyy-MM-dd'T'HH:mm:ss.SSSZ";
            };
            formatterParseBlocks[29] = ^(NSString *string) {
                return @"yyyy-MM-dd'T'HH:mm:ss.SSSZ";
            };
        }
        
        {
            /*
             Fri Sep 04 00:12:21 +0800 2015 // Weibo, Twitter
             Fri Sep 04 00:12:21.000 +0800 2015
             */
            formatterParseBlocks[30] = ^(NSString *string) {
                return @"EEE MMM dd HH:mm:ss Z yyyy";
            };
            formatterParseBlocks[34] = ^(NSString *string) {
                return @"EEE MMM dd HH:mm:ss.SSS Z yyyy";
            };
        }
    });
    
    if (!dateString || dateString.length > 34) {
        return nil;
    }
    YYYNSDateFormatterParseBlock parser = formatterParseBlocks[dateString.length];
    if (!parser)
        return nil;
    return parser(dateString);
}

@implementation NSDate (YYYAddition)

+ (NSDateFormatter *)conversionFromDateToStringFormatter {
    static NSDateFormatter *outputFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        outputFormatter = [NSDateFormatter new];
        outputFormatter.locale = [NSLocale currentLocale];
        outputFormatter.timeZone = [NSTimeZone localTimeZone];
    });
    return outputFormatter;
}

+ (NSDateFormatter *)conversionFromStringToDateFormatter {
    static NSDateFormatter *inputFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        inputFormatter = [NSDateFormatter new];
        inputFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        inputFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    });
    return inputFormatter;
}

#pragma mark - String To Date
+ (NSDate *)dateFromString:(NSString *)string withFormat:(NSString *)format {
    if (!string || ![string isKindOfClass:[NSString class]]) {
        return nil;
    }
    if (!format) {
        format = YYY_NSDateFormatterString(string);
        if (!format) {
            return nil;
        }
    }
    NSDateFormatter *formatter = [self conversionFromStringToDateFormatter];
    [formatter setDateFormat:format];
    return [formatter dateFromString:string];
}

+ (NSDate *)dateFromString:(NSString *)string {
    return [self dateFromString:string withFormat:nil];
}

+ (NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day {
    NSString *dateString = [NSString stringWithFormat:@"%@-%02d-%02d",@(year),(int)month,(int)day];
    return [self dateFromString:dateString withFormat:@"yyyy-MM-dd"];
}

#pragma mark - Date To String
- (NSString *)stringWithFormat:(NSString *)format {
    NSDateFormatter *outputFormatter = [NSDate conversionFromDateToStringFormatter];
    [outputFormatter setDateFormat:format];
    return [outputFormatter stringFromDate:self];
}

- (NSString *)dateString {
    return [self stringWithFormat:@"yyyy-MM-dd"];
}

- (NSString *)string {
    return [self stringWithFormat:@"yyyy-MM-dd HH:mm:ss"];
}

#pragma mark - Date Helper
- (NSDate *)dateByAddingDays:(NSInteger)days {
    NSTimeInterval aTimeInterval = [self timeIntervalSinceReferenceDate] + 86400 * days;
    NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
    return newDate;
}

- (NSDate *)beginningOfMonth {
    NSString *dateString = [NSString stringWithFormat:@"%@-01", [self stringWithFormat:@"yyyy-MM"]];
    return [NSDate dateFromString:dateString];
}

- (NSDate *)endOfMonth {
    NSString *dateString = [NSString stringWithFormat:@"%@-%@", [self stringWithFormat:@"yyyy-MM"], @(self.numberOfDaysInMonth)];
    return [NSDate dateFromString:dateString];
}

- (NSInteger)numberOfDaysInMonth {
    NSCalendar *calendar = YYY_Calendar_Gregorian();
    NSRange range = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:self];
    return range.length;
}

@end

NS_INLINE NSString *YYY_GanZhiString(NSInteger n) {
    NSString *ganString = [@"甲乙丙丁戊己庚辛壬葵" substringWithRange:NSMakeRange(n%10, 1)];
    NSString *zhiString = [@"子丑寅卯辰巳午未申酉戌亥" substringWithRange:NSMakeRange(n%12, 1)];
    return [ganString stringByAppendingString:zhiString];
}

NS_INLINE NSInteger YYY_sTerm(NSInteger year, NSInteger n) {
    static NSArray<NSNumber *> *sTermInfo_ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sTermInfo_ = @[@(0), @(21208), @(42467), @(63836), @(85337), @((107014)), @(128867), @(150921), @(173149), @(195551), @(218072), @(240693), @(263343), @(285989), @(308563), @(331033), @(353350), @(375494), @(397447), @(419210), @(440795), @(462224), @(483532), @(504758)];
    });
    CGFloat f = 31556925974.7 * (year - 1900) + sTermInfo_[n].doubleValue * 60000 -2208549300000;
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:f/1000];
    return date.day;
}

NS_INLINE NSCalendar *YYY_Calendar_Chinese() {
    static NSCalendar *yyy_calendar_chinese = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        yyy_calendar_chinese = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierChinese];
    });
    return yyy_calendar_chinese;
}

NS_INLINE NSString *YYY_LunarDay(NSInteger day){
    static NSArray<NSString *> *lunarDaysStrings__ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        lunarDaysStrings__ = [NSArray arrayWithObjects:
                              @"初一", @"初二", @"初三", @"初四", @"初五", @"初六", @"初七", @"初八", @"初九", @"初十",
                              @"十一", @"十二", @"十三", @"十四", @"十五", @"十六", @"十七", @"十八", @"十九", @"二十",
                              @"廿一", @"廿二", @"廿三", @"廿四", @"廿五", @"廿六", @"廿七", @"廿八", @"廿九", @"三十", nil];
    });
    day = MIN(day - 1, 30);
    return lunarDaysStrings__[day];
}

@implementation NSDate (ChineseZodiac)

- (NSDictionary *)chineseSexagenaryCycle {
    NSMutableDictionary *info = [NSMutableDictionary dictionary];
    NSCalendarUnit componentsUnit = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents *gregorianComp = [YYY_Calendar_Gregorian() components:componentsUnit fromDate:self];
    NSDateComponents *chineseComponets = [YYY_Calendar_Chinese() components:componentsUnit fromDate:self];
    NSInteger days = [YYY_Calendar_Chinese() rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:self].length;
    NSInteger y = gregorianComp.year;
    NSInteger m = gregorianComp.month - 1;
    NSInteger d = gregorianComp.day;
    
    NSInteger term2 = YYY_sTerm(y, 2);
    NSInteger firstNode = YYY_sTerm(y, m*2);
    NSDate *firstDate = [NSDate dateWithYear:y month:gregorianComp.month day:1];
    NSInteger dayCyclical = [firstDate timeIntervalSince1970] * 1000 / 86400000 + 25567 + 10;
    
    NSString *cY = nil;
    NSString *cM = nil;
    NSString *cD = nil;
    
    if (m < 2) {
        if (m == 1 && self.day == term2 ) {
            cY = YYY_GanZhiString(y - 1900 + 36);
        } else {
            cY = YYY_GanZhiString(y - 1900 + 36 - 1);
        }
    } else {
        cY = YYY_GanZhiString(y - 1900 + 36);
    }
    
    if (d >= firstNode) {
        cM = YYY_GanZhiString((y - 1900) * 12 + m + 13);
    } else {
        cM = YYY_GanZhiString((y - 1900) * 12 + m + 12);
    }
    cD = YYY_GanZhiString(dayCyclical + d - 1);
    
    NSString *lunarM = [@"正二三四五六七八九十冬腊月" substringWithRange:NSMakeRange(chineseComponets.month - 1, 1)];
    NSString *lunarD = YYY_LunarDay(chineseComponets.day);
    NSString *animal = [@"鼠牛虎兔龙蛇马羊猴鸡狗猪" substringWithRange:NSMakeRange((y - 4)%12, 1)];
    NSString *lunarSize = days > 29? @"大":@"小";
    [info setObject:cY forKey:@"year"];
    [info setObject:cM forKey:@"month"];
    [info setObject:cD forKey:@"day"];
    [info setObject:animal forKey:@"zodiac"];
    [info setObject:lunarM forKey:@"lunarMonth"];
    [info setObject:lunarD forKey:@"lunarDay"];
    [info setObject:lunarSize forKey:@"lunarSize"];
    return info;
}

@end
