//
//  NSDate+Addition.h
//  Pods
//
//  Created by 叶越悦 on 2017/7/25.
//
//

#import <Foundation/Foundation.h>

@interface NSDate (YYYProperty)

@property (nonatomic, readonly) NSInteger year; /**< 年 */
@property (nonatomic, readonly) NSInteger month; /**< 月 (1~12) */
@property (nonatomic, readonly) NSInteger day; /**< 日 (1~31) */
@property (nonatomic, readonly) NSInteger hour; /**< 小时 (0~23) */
@property (nonatomic, readonly) NSInteger minute; /**< 分钟 (0~59) */
@property (nonatomic, readonly) NSInteger second; /**< 秒 (0~59) */
@property (nonatomic, readonly) NSInteger nanosecond; /**< 纳秒 */
@property (nonatomic, readonly) NSInteger weekday; /**< 周几  (1~7, 第一天是基于用户设置的,默认周日第一天) */
@property (nonatomic, readonly) NSInteger weekdayOrdinal; /**< WeekdayOrdinal */
@property (nonatomic, readonly) NSInteger weekOfMonth; /**< WeekOfMonth component (1~5) */
@property (nonatomic, readonly) NSInteger weekOfYear; /**< WeekOfYear component (1~53) */
@property (nonatomic, readonly) NSInteger yearForWeekOfYear; /**< YearForWeekOfYear component */
@property (nonatomic, readonly) NSInteger quarter; ///< Quarter component
@property (nonatomic, readonly) BOOL isLeapMonth; ///< Weather the month is leap month
@property (nonatomic, readonly) BOOL isLeapYear; ///< Weather the year is leap year
@property (nonatomic, readonly) BOOL isToday; ///< Weather date is today (based on current locale)

@end

@interface NSDate (YYYAddition)

/**
 NSDate 转换 NSString 使用的 NSDateFormatter
 默认 [NSLocale currentLocale];
 默认 [NSTimeZone localTimeZone];

 @return NSDateFormatter
 */
+ (NSDateFormatter *)conversionFromDateToStringFormatter;

/**
 NSString 转换 NSDate 使用的 NSDateFormatter
 默认 [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
 默认 [NSTimeZone timeZoneForSecondsFromGMT:0];
 
 @return NSDateFormatter
 */
+ (NSDateFormatter *)conversionFromStringToDateFormatter;


/**
 使用 conversionFromStringToDateFormatter 进行转换

 @param string 日期字符串
 @param format 格式
 @return NSDate
 */
+ (NSDate *)dateFromString:(NSString *)string withFormat:(NSString *)format;

/**
 [NSDate dateFromString:string withFormat:nil]

 @param string 日期字符串
 @return NSDate
 */
+ (NSDate *)dateFromString:(NSString *)string;
+ (NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day;

/**
 使用 conversionFromDateToStringFormatter 进行转换
 
 @param format 转换格式
 @return string
 */
- (NSString *)stringWithFormat:(NSString *)format;

/**
 [self stringWithFormat:@"yyyy-MM-dd"]

 @return string
 */
- (NSString *)dateString;

/**
 [self stringWithFormat:@"yyyy-MM-dd HH:mm:ss"]

 @return string
 */
- (NSString *)string;

- (NSDate *)dateByAddingDays:(NSInteger)days;

/**
 返回该月的第一天

 @return NSDate
 */
- (NSDate *)beginningOfMonth;

/**
 返回该月的最后一天

 @return NSDate
 */
- (NSDate *)endOfMonth;


/**
 返回该月的总共天数

 @return 天数
 */
- (NSInteger)numberOfDaysInMonth;

@end


@interface NSDate (ChineseZodiac)

/**
 中国的天干地支，生肖，农历月份日期

 @return 字典
 */
- (NSDictionary *)chineseSexagenaryCycle;

@end
