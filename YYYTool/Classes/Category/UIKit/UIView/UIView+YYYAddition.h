//
//  UIView+SetFrameAddition.h
//  BossmailQ
//
//  Created by chen shengzhi on 12-11-5.
//  Copyright (c) 2012年 zzy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (YYYAddition)

@property (nonatomic, assign) CGFloat cornerRadius;/**< 圆角 */
- (void)setBorderWidth:(CGFloat)width andBorderColor:(UIColor *)color;

/**
 *  @author 叶越悦, 15-08-10 10:32:22
 *
 *  设置单边圆角
 *
 *  @param corners     UIRectCorner
 *  @param cornerRadii 圆角值
 */
- (void)setRoundingCorners:(UIRectCorner)corners cornerRadii:(CGSize)cornerRadii;

- (__kindof UIView *)viewMatchingPredicate:(NSPredicate *)predicate;
- (__kindof UIView *)viewWithTag:(NSInteger)tag ofClass:(Class)viewClass;
- (__kindof UIView *)viewOfClass:(Class)viewClass;

- (NSArray<__kindof UIView *> *)viewsMatchingPredicate:(NSPredicate *)predicate;
- (NSArray<__kindof UIView *> *)viewsWithTag:(NSInteger)tag;
- (NSArray<__kindof UIView *> *)viewsWithTag:(NSInteger)tag ofClass:(Class)viewClass;
- (NSArray<__kindof UIView *> *)viewsOfClass:(Class)viewClass;

- (__kindof UIView *)firstSuperviewMatchingPredicate:(NSPredicate *)predicate;
- (__kindof UIView *)firstSuperviewOfClass:(Class)viewClass;
- (__kindof UIView *)firstSuperviewWithTag:(NSInteger)tag;
- (__kindof UIView *)firstSuperviewWithTag:(NSInteger)tag ofClass:(Class)viewClass;

- (BOOL)viewOrAnySuperviewMatchesPredicate:(NSPredicate *)predicate;
- (BOOL)viewOrAnySuperviewIsKindOfClass:(Class)viewClass;
- (BOOL)isSuperviewOfView:(UIView *)view;
- (BOOL)isSubviewOfView:(UIView *)view;

- (UIViewController *)firstViewController;
- (__kindof UIView *)firstResponder;

@end

@interface UIView (SetFrame)

@property (nonatomic) CGFloat f_left;
@property (nonatomic) CGFloat f_top;
@property (nonatomic) CGFloat f_right;
@property (nonatomic) CGFloat f_bottom;
@property (nonatomic) CGFloat f_width;
@property (nonatomic) CGFloat f_height;
@property (nonatomic) CGFloat f_centerX;
@property (nonatomic) CGFloat f_centerY;
@property (nonatomic) CGPoint f_origin;
@property (nonatomic) CGSize f_size;
@property (nonatomic, readonly) UIEdgeInsets f_safeAreaInsets;
// 修改以下值 只会进行视图移动，不会修改 视图大小
@property (nonatomic) CGFloat maxX;
@property (nonatomic) CGFloat minX;
@property (nonatomic) CGFloat minY;
@property (nonatomic) CGFloat maxY;

@end

@interface UIView (SetBounds)

@property (nonatomic) CGFloat b_left;
@property (nonatomic) CGFloat b_top;
@property (nonatomic) CGFloat b_right;
@property (nonatomic) CGFloat b_bottom;
@property (nonatomic) CGFloat b_width;
@property (nonatomic) CGFloat b_height;
@property (nonatomic, readonly) CGFloat b_centerX;
@property (nonatomic, readonly) CGFloat b_centerY;
@property (nonatomic, readonly) CGPoint b_center;
@property (nonatomic) CGPoint b_origin;
@property (nonatomic) CGSize b_size;

@end

@interface UIView (YYYConstraints)

@property (nonatomic, assign) BOOL isSetupConstraints;

/**
 可以在这边添加子View的约束，这个会在第一次调用updateConstraints的时候执行一次。
 */
- (void)yyy_setupViewsConstraints NS_REQUIRES_SUPER;

@end
