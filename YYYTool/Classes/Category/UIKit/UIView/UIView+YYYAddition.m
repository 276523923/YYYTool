//
//  UIView+SetFrameAddition.m
//  BossmailQ
//
//  Created by chen shengzhi on 12-11-5.
//  Copyright (c) 2012年 zzy. All rights reserved.
//

#import "UIView+YYYAddition.h"
#import <objc/runtime.h>
#import "NSObject+YYYAddition.h"

@implementation UIView (YYYAddition)

- (CGFloat)cornerRadius {
    return self.layer.cornerRadius;
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    self.layer.cornerRadius = cornerRadius;
    self.layer.masksToBounds = YES;
}

/**
 *  @author 叶越悦, 15-07-24 09:09:04
 *
 *  设置边框宽度与颜色
 *
 *  @param width 宽度
 *  @param color 颜色
 */
- (void)setBorderWidth:(CGFloat)width andBorderColor:(UIColor *)color {
    self.layer.borderWidth = width;
    self.layer.borderColor = color.CGColor;
}

/**
 *  @author 叶越悦, 15-08-10 10:32:22
 *
 *  设置单边圆角
 *
 *  @param corners     要设置的圆角
 *  @param cornerRadii 圆角值
 */
- (void)setRoundingCorners:(UIRectCorner)corners cornerRadii:(CGSize)cornerRadii {
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:cornerRadii];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
    self.layer.masksToBounds = YES;
}

- (UIView *)viewMatchingPredicate:(NSPredicate *)predicate {
    if ([predicate evaluateWithObject:self]) {
        return self;
    }
    for (UIView *view in self.subviews) {
        UIView *match = [view viewMatchingPredicate:predicate];
        if (match)
            return match;
    }
    return nil;
}

- (UIView *)viewWithTag:(NSInteger)tag ofClass:(Class)viewClass {
    return [self viewMatchingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, __unused NSDictionary *bindings) {
        return [evaluatedObject tag] == tag && [evaluatedObject isKindOfClass:viewClass];
    }]];
}

- (UIView *)viewOfClass:(Class)viewClass {
    return [self viewMatchingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, __unused NSDictionary *bindings) {
        return [evaluatedObject isKindOfClass:viewClass];
    }]];
}

- (NSArray *)viewsMatchingPredicate:(NSPredicate *)predicate {
    NSMutableArray *matches = [NSMutableArray array];
    if ([predicate evaluateWithObject:self]) {
        [matches addObject:self];
    }
    for (UIView *view in self.subviews) {
        //check for subviews
        //avoid creating unnecessary array
        if ([view.subviews count]) {
            [matches addObjectsFromArray:[view viewsMatchingPredicate:predicate]];
        } else if ([predicate evaluateWithObject:view]) {
            [matches addObject:view];
        }
    }
    return matches;
}

- (NSArray *)viewsWithTag:(NSInteger)tag {
    return [self viewsMatchingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, __unused id bindings) {
        return [evaluatedObject tag] == tag;
    }]];
}

- (NSArray *)viewsWithTag:(NSInteger)tag ofClass:(Class)viewClass {
    return [self viewsMatchingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, __unused id bindings) {
        return [evaluatedObject tag] == tag && [evaluatedObject isKindOfClass:viewClass];
    }]];
}

- (NSArray *)viewsOfClass:(Class)viewClass {
    return [self viewsMatchingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, __unused id bindings) {
        return [evaluatedObject isKindOfClass:viewClass];
    }]];
}

- (UIView *)firstSuperviewMatchingPredicate:(NSPredicate *)predicate {
    if ([predicate evaluateWithObject:self]) {
        return self;
    }
    return [self.superview firstSuperviewMatchingPredicate:predicate];
}

- (UIView *)firstSuperviewOfClass:(Class)viewClass {
    return [self firstSuperviewMatchingPredicate:[NSPredicate predicateWithBlock:^BOOL(UIView *superview, __unused id bindings) {
        return [superview isKindOfClass:viewClass];
    }]];
}

- (UIView *)firstSuperviewWithTag:(NSInteger)tag {
    return [self firstSuperviewMatchingPredicate:[NSPredicate predicateWithBlock:^BOOL(UIView *superview, __unused id bindings) {
        return superview.tag == tag;
    }]];
}

- (UIView *)firstSuperviewWithTag:(NSInteger)tag ofClass:(Class)viewClass {
    return [self firstSuperviewMatchingPredicate:[NSPredicate predicateWithBlock:^BOOL(UIView *superview, __unused id bindings) {
        return superview.tag == tag && [superview isKindOfClass:viewClass];
    }]];
}

- (BOOL)viewOrAnySuperviewMatchesPredicate:(NSPredicate *)predicate {
    if ([predicate evaluateWithObject:self]) {
        return YES;
    }
    return [self.superview viewOrAnySuperviewMatchesPredicate:predicate];
}

- (BOOL)viewOrAnySuperviewIsKindOfClass:(Class)viewClass {
    return [self viewOrAnySuperviewMatchesPredicate:[NSPredicate predicateWithBlock:^BOOL(UIView *superview, __unused id bindings) {
        return [superview isKindOfClass:viewClass];
    }]];
}

- (BOOL)isSuperviewOfView:(UIView *)view {
    return [self firstSuperviewMatchingPredicate:[NSPredicate predicateWithBlock:^BOOL(UIView *superview, __unused id bindings) {
        return superview == view;
    }]] != nil;
}

- (BOOL)isSubviewOfView:(UIView *)view {
    return [view isSuperviewOfView:self];
}

//responder chain

- (UIViewController *)firstViewController {
    for (UIView *view = self; view; view = view.superview) {
        UIResponder *nextResponder = [view nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *) nextResponder;
        }
    }
    return nil;
}

- (UIView *)firstResponder {
    return [self viewMatchingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, __unused id bindings) {
        return [evaluatedObject isFirstResponder];
    }]];
}

@end

@implementation UIView (SetFrame)

- (CGFloat)f_left {
    return CGRectGetMinX(self.frame);
}

- (void)setF_left:(CGFloat)f_left {
    CGRect frame = self.frame;
    frame.origin.x = f_left;
    self.frame = frame;
}

- (CGFloat)f_top {
    return CGRectGetMinY(self.frame);
}

- (void)setF_top:(CGFloat)y {
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)f_right {
    return CGRectGetMaxX(self.frame);
}

- (void)setF_right:(CGFloat)f_right {
    self.f_left = f_right - self.f_width;
}

- (CGFloat)f_bottom {
    return CGRectGetMaxY(self.frame);
}

- (void)setF_bottom:(CGFloat)f_bottom {
    self.f_top = f_bottom - self.f_height;
}

- (CGFloat)f_width {
    return CGRectGetWidth(self.frame);
}

- (void)setF_width:(CGFloat)f_width {
    CGRect frame = self.frame;
    frame.size.width = f_width;
    self.frame = frame;
}

- (CGFloat)f_height {
    return CGRectGetHeight(self.frame);
}

- (void)setF_height:(CGFloat)f_height {
    CGRect frame = self.frame;
    frame.size.height = f_height;
    self.frame = frame;
}

- (CGFloat)f_centerX {
    return self.center.x;
}

- (void)setF_centerX:(CGFloat)f_centerX {
    self.center = CGPointMake(f_centerX, self.center.y);
}

- (CGFloat)f_centerY {
    return self.center.y;
}

- (void)setF_centerY:(CGFloat)f_centerY {
    self.center = CGPointMake(self.center.x, f_centerY);
}

- (CGPoint)f_origin {
    return self.frame.origin;
}

- (void)setF_origin:(CGPoint)origin {
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (CGSize)f_size {
    return self.frame.size;
}

- (void)setF_size:(CGSize)size {
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (UIEdgeInsets)f_safeAreaInsets {
    if (@available(iOS 11, *)) {
        return self.safeAreaInsets;
    } else {
        return UIEdgeInsetsZero;
    }
}

- (CGFloat)minX {
    return self.f_left;
}

- (CGFloat)maxX {
    return self.f_right;
}

- (CGFloat)minY {
    return self.f_top;
}

- (CGFloat)maxY {
    return self.f_bottom;
}

- (void)setMinX:(CGFloat)minX {
    self.f_left = minX;
}

- (void)setMaxX:(CGFloat)maxX {
    self.f_right = maxX;
}

- (void)setMinY:(CGFloat)minY {
    self.f_top = minY;
}

- (void)setMaxY:(CGFloat)maxY {
    self.f_bottom = maxY;
}

@end

@implementation UIView (SetBounds)


- (CGFloat)b_left {
    return CGRectGetMinX(self.bounds);
}

- (void)setB_left:(CGFloat)b_left {
    CGRect bounds = self.bounds;
    bounds.origin.x = b_left;
    self.bounds = bounds;
}

- (CGFloat)b_top {
    return CGRectGetMinY(self.bounds);
}

- (void)setB_top:(CGFloat)y {
    CGRect bounds = self.bounds;
    bounds.origin.y = y;
    self.bounds = bounds;
}

- (CGFloat)b_right {
    return CGRectGetMaxX(self.bounds);
}

- (void)setB_right:(CGFloat)b_right {
    self.b_left = b_right - self.b_width;
}

- (CGFloat)b_bottom {
    return CGRectGetMaxY(self.bounds);
}

- (void)setB_bottom:(CGFloat)b_bottom {
    self.b_top = b_bottom - self.b_height;
}

- (CGFloat)b_width {
    return CGRectGetWidth(self.bounds);
}

- (void)setB_width:(CGFloat)b_width {
    CGRect bounds = self.bounds;
    bounds.size.width = b_width;
    self.bounds = bounds;
}

- (CGFloat)b_height {
    return CGRectGetHeight(self.bounds);
}

- (void)setB_height:(CGFloat)b_height {
    CGRect bounds = self.bounds;
    bounds.size.height = b_height;
    self.bounds = bounds;
}

- (CGFloat)b_centerX {
    return self.b_width/2;
}

- (CGFloat)b_centerY {
    return self.b_height/2;
}

- (CGPoint)b_center {
    return CGPointMake(self.b_centerX, self.b_centerY);
}

- (CGPoint)b_origin {
    return self.bounds.origin;
}

- (void)setB_origin:(CGPoint)origin {
    CGRect bounds = self.bounds;
    bounds.origin = origin;
    self.bounds = bounds;
}

- (CGSize)b_size {
    return self.bounds.size;
}

- (void)setB_size:(CGSize)size {
    CGRect bounds = self.bounds;
    bounds.size = size;
    self.bounds = bounds;
}

@end

@implementation UIView (YYYConstraints)

+ (void)load {
    [UIView swizzleSelector:@selector(updateConstraints) swapSelector:@selector(yyy_updateConstraints)];
}

- (void)yyy_updateConstraints {
    [self yyy_updateConstraints];
    if (!self.isSetupConstraints) {
        [self yyy_setupViewsConstraints];
        self.isSetupConstraints = YES;
    }
}

- (void)yyy_setupViewsConstraints {
    self.isSetupConstraints = YES;
}

- (void)setIsSetupConstraints:(BOOL)isSetupConstraints {
    objc_setAssociatedObject(self, @selector(isSetupConstraints), @(isSetupConstraints), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)isSetupConstraints {
    return [objc_getAssociatedObject(self, @selector(isSetupConstraints)) boolValue];
}

@end
