//
//  UITextField+Addition.h
//  BaoZhangWang
//
//  Created by 叶越悦 on 15/10/13.
//  Copyright © 2015年 BaoliNetworkTechnology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (YYYAddition)

@property (nonatomic, assign) UIEdgeInsets textRectInsets;
@property (nonatomic, strong, readonly) UILabel *yyy_placeholderLabel;

/**
 *  @author 叶越悦, 15-10-13 14:53:23
 *
 *  快速创建TextField
 *
 *  @return TextField
 */
+ (instancetype)createTextField;

/**
 *  @author 叶越悦, 15-10-13 14:53:47
 *
 *  快速创建TextField - 带 placeholder
 *
 *  @param placeholder placeholder
 *
 *  @return TextFile
 */
+ (instancetype)createTextFieldWithPlaceholder:(NSString *)placeholder;

/**
 *  @author 叶越悦, 15-10-13 14:54:14
 *
 *  快速创建TextField - 带 text
 *
 *  @param text text
 *
 *  @return TextFile
 */
+ (instancetype)createTextFieldWithDefaultText:(NSString *)text;

@end

//输入类型
typedef NS_ENUM(NSUInteger, TextFieldInputType) {
    TextFieldInputTypeAll,       //任意
    TextFieldInputTypeIntOnly,   //整形
    TextFieldInputTypeDoubleOnly, //浮点型
};

/**
 *  限制UITextField的输入
 */
@interface UITextField (InputLimit)

@property (nonatomic, assign) NSInteger maxLength; /**< 输入长度 */
@property (nonatomic, assign) NSInteger decimalDigits; /**< 小数位数，设置的话会将inputType设置为TextFieldInputTypeDoubleOnly */
@property (nonatomic, assign) TextFieldInputType inputType; /**< 输入类型 */

@end

