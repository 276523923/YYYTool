//
//  UIViewController+AutoKeyBoardHidden.h
//  BaoZhangWang
//
//  Created by yyy on 16/7/11.
//  Copyright © 2016年 yyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (YYYAddition)

@property (nonatomic, assign) BOOL yyy_autoHiddenKeyBoardWhenTapView;/**< 点击view自动隐藏键盘 */
@property (nonatomic) UIStatusBarStyle yyy_statusBarStyle;

@end
