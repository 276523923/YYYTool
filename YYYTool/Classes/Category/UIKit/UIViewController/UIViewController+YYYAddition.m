//
//  UIViewController+AutoKeyBoardHidden.m
//  BaoZhangWang
//
//  Created by yyy on 16/7/11.
//  Copyright © 2016年 yyy. All rights reserved.
//

#import "UIViewController+YYYAddition.h"
#import <objc/runtime.h>
#import "NSObject+YYYAddition.h"

//防止UITextField点击清空按钮的时候隐藏了键盘
@interface MyTapGestureRecognizerDelegate : NSObject <UIGestureRecognizerDelegate>

@end

@implementation MyTapGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isKindOfClass:[UIButton class]] &&
        touch.view.superview &&
        [touch.view.superview isKindOfClass:[UITextField class]]) {
        return NO;
    }
    return YES;
}

@end

@interface UIViewController ()

@property (nonatomic, strong, readonly) UITapGestureRecognizer *hiddenKeyBoardTapGestureRecognizer;/**< 点击隐藏键盘手势 */
@property (nonatomic, strong) MyTapGestureRecognizerDelegate *myTapGestureRecognizerDelegate;
@property (nonatomic, assign) BOOL isRegisterKeyBoardNot;/**< 是否注册键盘通知 */

@end

@implementation UIViewController (YYYAddition)

- (UIStatusBarStyle)preferredStatusBarStyle {
    return self.yyy_statusBarStyle;
}

#pragma mark - keyBoard Handle

- (void)yyy_registerKeyBoardNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(yyy_keyBoardShowHandle) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(yyy_keyBoardHiddenHandle) name:UIKeyboardDidHideNotification object:nil];
}

- (void)yyy_unregisterKeyBoardNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    [self.view removeGestureRecognizer:self.hiddenKeyBoardTapGestureRecognizer];
}

- (void)yyy_keyBoardShowHandle {
    [self.view addGestureRecognizer:self.hiddenKeyBoardTapGestureRecognizer];
}

- (void)yyy_keyBoardHiddenHandle {
    [self.view removeGestureRecognizer:self.hiddenKeyBoardTapGestureRecognizer];
}

- (void)yyy_hiddenKeyBoardNot:(UITapGestureRecognizer *)tap {
    //有取消隐藏的机会
    [self.view performSelector:@selector(endEditing:) withObject:@(YES) afterDelay:.1];
}

#pragma mark - get, set

- (BOOL)yyy_autoHiddenKeyBoardWhenTapView {
    return [objc_getAssociatedObject(self, @selector(yyy_autoHiddenKeyBoardWhenTapView)) boolValue];
}

- (void)setYyy_autoHiddenKeyBoardWhenTapView:(BOOL)autoHiddenKeyBoardWhenTapView {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [UIViewController swizzleSelector:@selector(viewWillAppear:) swapSelector:@selector(yyy_keyboard_viewWillAppear:)];
        [UIViewController swizzleSelector:@selector(viewWillDisappear:) swapSelector:@selector(yyy_keyboard_viewWillDisappear:)];
    });
    self.isRegisterKeyBoardNot = autoHiddenKeyBoardWhenTapView;
    objc_setAssociatedObject(self, @selector(yyy_autoHiddenKeyBoardWhenTapView), @(autoHiddenKeyBoardWhenTapView), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UITapGestureRecognizer *)hiddenKeyBoardTapGestureRecognizer {
    UITapGestureRecognizer *tap = objc_getAssociatedObject(self, @selector(hiddenKeyBoardTapGestureRecognizer));
    if (!tap) {
        tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(yyy_hiddenKeyBoardNot:)];
        tap.cancelsTouchesInView = NO;
        tap.delegate = self.myTapGestureRecognizerDelegate;
        objc_setAssociatedObject(self, @selector(hiddenKeyBoardTapGestureRecognizer), tap, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return tap;
}

- (MyTapGestureRecognizerDelegate *)myTapGestureRecognizerDelegate {
    MyTapGestureRecognizerDelegate *delegate = objc_getAssociatedObject(self, @selector(myTapGestureRecognizerDelegate));
    if (!delegate) {
        delegate = [MyTapGestureRecognizerDelegate new];
        objc_setAssociatedObject(self, @selector(myTapGestureRecognizerDelegate), delegate, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return delegate;
}

- (BOOL)isRegisterKeyBoardNot {
    return [objc_getAssociatedObject(self, @selector(isRegisterKeyBoardNot)) boolValue];
}

- (void)setIsRegisterKeyBoardNot:(BOOL)isRegisterKeyBoardNot {
    if (self.isRegisterKeyBoardNot == isRegisterKeyBoardNot) {
        return;
    } else {
        objc_setAssociatedObject(self, @selector(isRegisterKeyBoardNot), @(isRegisterKeyBoardNot), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        if (isRegisterKeyBoardNot) {
            [self yyy_registerKeyBoardNotification];
        } else {
            [self yyy_unregisterKeyBoardNotification];
        }
    }
}

- (void)yyy_keyboard_viewWillAppear:(BOOL)animated {
    [self yyy_keyboard_viewWillAppear:animated];
    self.isRegisterKeyBoardNot = self.yyy_autoHiddenKeyBoardWhenTapView;
}

- (void)yyy_keyboard_viewWillDisappear:(BOOL)animated {
    [self yyy_keyboard_viewWillDisappear:animated];
    self.isRegisterKeyBoardNot = NO;
}

- (UIStatusBarStyle)yyy_statusBarStyle {
    NSNumber *style = objc_getAssociatedObject(self, @selector(yyy_statusBarStyle));
    if (!style) {
        NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
        NSString *infostyle = [info valueForKey:@"UIStatusBarStyle"];
        if (infostyle && [infostyle isEqualToString:@"UIStatusBarStyleLightContent"]) {
            style = @(UIStatusBarStyleLightContent);
        } else {
            style = @(UIStatusBarStyleDefault);
        }
        objc_setAssociatedObject(self, @selector(yyy_statusBarStyle), style, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return [style integerValue];
}

- (void)setYyy_statusBarStyle:(UIStatusBarStyle)yyy_statusBarStyle {
    UIStatusBarStyle style = self.yyy_statusBarStyle;
    if (style == yyy_statusBarStyle) {
        return;
    }
    objc_setAssociatedObject(self, @selector(yyy_statusBarStyle), @(yyy_statusBarStyle), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self setNeedsStatusBarAppearanceUpdate];
}

@end
