//
//  UICollectionViewCell+YYYAddition.h
//  Pods-YYYTool_Example
//
//  Created by 叶越悦 on 2018/4/24.
//

#import <UIKit/UIKit.h>

@interface UICollectionViewCell (YYYAddition)

- (void)setupInfo:(id)info;
- (void)setupInfo:(id)info delegate:(id)delegate;

+ (CGSize)itemSize;
+ (CGSize)itemSizeWithInfo:(id)info;

@end
