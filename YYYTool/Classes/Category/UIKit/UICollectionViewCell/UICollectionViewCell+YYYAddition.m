//
//  UICollectionViewCell+YYYAddition.m
//  Pods-YYYTool_Example
//
//  Created by 叶越悦 on 2018/4/24.
//

#import "UICollectionViewCell+YYYAddition.h"

@implementation UICollectionViewCell (YYYAddition)

- (void)setupInfo:(id)info {
}

- (void)setupInfo:(id)info delegate:(id)delegate {
    [self setupInfo:info];
}

+ (CGSize)itemSize {
    return CGSizeMake(50, 50);
}

+ (CGSize)itemSizeWithInfo:(id)info {
    return [self itemSize];
}

@end
