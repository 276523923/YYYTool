//
//  UIImage+Addition.m
//  Pods
//
//  Created by 叶越悦 on 2017/7/21.
//
//

#import "UIImage+YYYAddition.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIImage (YYYAddition)

+ (UIImage *)yyy_imageFromView:(UIView *)view {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, YES, [[UIScreen mainScreen] scale]);
    BOOL hidden = [view isHidden];
    [view setHidden:NO];
    [[view layer] renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [view setHidden:hidden];
    return image;
}

+ (instancetype)yyy_templateImageWithImageName:(NSString *)name {
    return [[self imageNamed:name] yyy_renderingModeAlwaysTemplateImage];
}

+ (instancetype)yyy_OriginalImageWithImageName:(NSString *)name {
    return [[self imageNamed:name] yyy_renderingModeAlwaysOriginalImage];
}

- (instancetype)yyy_renderingModeAlwaysOriginalImage {
    return [self imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}

- (instancetype)yyy_renderingModeAlwaysTemplateImage {
    return [self imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

@end
