//
//  UITableView+RegisterClass.m
//  BaoZhangWang
//
//  Created by 叶越悦 on 15/7/22.
//  Copyright (c) 2015年 BaoliNetworkTechnology. All rights reserved.
//

#import "UITableView+YYYAddition.h"
#import <objc/runtime.h>
#import "NSDictionary+YYYAddition.h"

static const void *allCellReuseIdentifierKey = &allCellReuseIdentifierKey;

@interface UITableView ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *allCellReuseIdentifier;

@end

@implementation UITableView (YYYAddition)

- (void)registerClass:(Class)cellClass {
    if (!cellClass) {
        return;
    }
    NSString *key = NSStringFromClass(cellClass);
    if ([self.allCellReuseIdentifier objectForKey:key]) {
        return;
    }
    [self.allCellReuseIdentifier setObject:cellClass forKey:key];
    [self registerClass:cellClass forCellReuseIdentifier:key];
}

- (UITableViewCell *)dequeueReusableCellWithCellClass:(Class)cellClass forIndexPath:(NSIndexPath *)indexPath {
    [self registerClass:cellClass];
    return [self dequeueReusableCellWithIdentifier:NSStringFromClass(cellClass) forIndexPath:indexPath];
}

- (void)setAllCellReuseIdentifier:(NSDictionary *)allCellReuseIdentifier {
    objc_setAssociatedObject(self, @selector(allCellReuseIdentifier), allCellReuseIdentifier, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSMutableDictionary *)allCellReuseIdentifier {
    NSMutableDictionary *dic = objc_getAssociatedObject(self, @selector(allCellReuseIdentifier));
    if (!dic) {
        dic = [NSMutableDictionary dictionary];
        self.allCellReuseIdentifier = dic;
    }
    return dic;
}

@end
