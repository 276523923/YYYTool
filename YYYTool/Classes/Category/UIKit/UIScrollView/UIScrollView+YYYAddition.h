//
//  UIScrollView+Addition.h
//  Pods
//
//  Created by 叶越悦 on 2017/6/19.
//
//

#import <UIKit/UIKit.h>

@interface UIScrollView (YYYAddition)

@property (nonatomic) CGFloat yyy_insetTop;
@property (nonatomic) CGFloat yyy_insetLeft;
@property (nonatomic) CGFloat yyy_insetBottom;
@property (nonatomic) CGFloat yyy_insetRight;
@property (nonatomic) CGFloat yyy_offsetX;
@property (nonatomic) CGFloat yyy_offsetY;
@property (nonatomic) CGFloat yyy_contentWidth;
@property (nonatomic) CGFloat yyy_contentHeight;

@property (nonatomic, readonly) UIEdgeInsets yyy_realContentInset;

/**
 Scroll content to top with animation.
 */
- (void)scrollToTop;

/**
 Scroll content to bottom with animation.
 */
- (void)scrollToBottom;

/**
 Scroll content to left with animation.
 */
- (void)scrollToLeft;

/**
 Scroll content to right with animation.
 */
- (void)scrollToRight;

/**
 Scroll content to top.
 
 @param animated  Use animation.
 */
- (void)scrollToTopAnimated:(BOOL)animated;

/**
 Scroll content to bottom.
 
 @param animated  Use animation.
 */
- (void)scrollToBottomAnimated:(BOOL)animated;

/**
 Scroll content to left.
 
 @param animated  Use animation.
 */
- (void)scrollToLeftAnimated:(BOOL)animated;

/**
 Scroll content to right.
 
 @param animated  Use animation.
 */
- (void)scrollToRightAnimated:(BOOL)animated;


@end
