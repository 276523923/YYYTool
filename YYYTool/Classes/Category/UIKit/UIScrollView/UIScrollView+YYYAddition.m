//
//  UIScrollView+Addition.m
//  Pods
//
//  Created by 叶越悦 on 2017/6/19.
//
//

#import "UIScrollView+YYYAddition.h"

@implementation UIScrollView (YYYAddition)

- (void)scrollToTop {
    [self scrollToTopAnimated:YES];
}

- (void)scrollToBottom {
    [self scrollToBottomAnimated:YES];
}

- (void)scrollToLeft {
    [self scrollToLeftAnimated:YES];
}

- (void)scrollToRight {
    [self scrollToRightAnimated:YES];
}

- (void)scrollToTopAnimated:(BOOL)animated {
    CGPoint off = self.contentOffset;
    off.y = 0 - self.contentInset.top;
    [self setContentOffset:off animated:animated];
}

- (void)scrollToBottomAnimated:(BOOL)animated {
    CGPoint off = self.contentOffset;
    off.y = self.contentSize.height - self.bounds.size.height + self.contentInset.bottom;
    [self setContentOffset:off animated:animated];
}

- (void)scrollToLeftAnimated:(BOOL)animated {
    CGPoint off = self.contentOffset;
    off.x = 0 - self.contentInset.left;
    [self setContentOffset:off animated:animated];
}

- (void)scrollToRightAnimated:(BOOL)animated {
    CGPoint off = self.contentOffset;
    off.x = self.contentSize.width - self.bounds.size.width + self.contentInset.right;
    [self setContentOffset:off animated:animated];
}

#pragma mark - set,set

- (void)setYyy_insetTop:(CGFloat)yyy_insetTop {
    UIEdgeInsets inset = self.contentInset;
    inset.top = yyy_insetTop;
    [self setContentInset:inset];
}

- (CGFloat)yyy_insetTop {
    return self.contentInset.top;
}

- (void)setYyy_insetLeft:(CGFloat)yyy_insetLeft {
    UIEdgeInsets inset = self.contentInset;
    inset.left = yyy_insetLeft;
    [self setContentInset:inset];
}

- (CGFloat)yyy_insetLeft {
    return self.contentInset.left;
}

- (void)setYyy_insetBottom:(CGFloat)yyy_insetBottom {
    UIEdgeInsets inset = self.contentInset;
    inset.bottom = yyy_insetBottom;
    [self setContentInset:inset];
}

- (CGFloat)yyy_insetBottom {
    return self.contentInset.bottom;
}

- (void)setYyy_insetRight:(CGFloat)yyy_insetRight {
    UIEdgeInsets inset = self.contentInset;
    inset.right = yyy_insetRight;
    [self setContentInset:inset];
}

- (CGFloat)yyy_insetRight {
    return self.contentInset.right;
}

- (void)setYyy_offsetX:(CGFloat)yyy_offsetX {
    CGPoint offset = self.contentOffset;
    offset.x = yyy_offsetX;
    [self setContentOffset:offset];
}

- (CGFloat)yyy_offsetX {
    return self.contentOffset.x;
}

- (void)setYyy_offsetY:(CGFloat)yyy_offsetY {
    CGPoint offset = self.contentOffset;
    offset.y = yyy_offsetY;
    [self setContentOffset:offset];
}

- (CGFloat)yyy_offsetY {
    return self.contentOffset.y;
}

- (void)setYyy_contentWidth:(CGFloat)yyy_contentWidth {
    CGSize size = self.contentSize;
    size.width = yyy_contentWidth;
    [self setContentSize:size];
}

- (CGFloat)yyy_contentWidth {
    return self.contentSize.width;
}

- (void)setYyy_contentHeight:(CGFloat)yyy_contentHeight {
    CGSize size = self.contentSize;
    size.height = yyy_contentHeight;
    [self setContentSize:size];
}

- (CGFloat)yyy_contentHeight {
    return self.contentSize.height;
}

- (UIEdgeInsets)yyy_realContentInset {
    if (@available(iOS 11, *)) {
        return self.adjustedContentInset;
    } else {
        return self.contentInset;
    }
}

@end
