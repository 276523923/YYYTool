//
//  UICollectionView+RegisterClass.m
//  BaoZhangWang
//
//  Created by 叶越悦 on 15/7/22.
//  Copyright (c) 2015年 BaoliNetworkTechnology. All rights reserved.
//

#import "UICollectionView+YYYAddition.h"
#import <objc/runtime.h>
#import "NSDictionary+YYYAddition.h"

@interface UICollectionView ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *allCellReuseIdentifier;

@end

@implementation UICollectionView (YYYAddition)

- (void)registerClass:(Class)cellClass {
    if (!cellClass) {
        return;
    }
    NSString *key = NSStringFromClass(cellClass);
    if ([self.allCellReuseIdentifier objectForKey:key]) {
        return;
    }
    [self.allCellReuseIdentifier setObject:cellClass forKey:key];
    [self registerClass:cellClass forCellWithReuseIdentifier:key];
}

- (UICollectionViewCell *)dequeueReusableCellWithCellClass:(Class)cellClass forIndexPath:(NSIndexPath *)indexPath {
    [self registerClass:cellClass];
    return [self dequeueReusableCellWithReuseIdentifier:NSStringFromClass(cellClass) forIndexPath:indexPath];
}

- (void)setAllCellReuseIdentifier:(NSMutableDictionary *)allCellReuseIdentifier {
    objc_setAssociatedObject(self, @selector(allCellReuseIdentifier), allCellReuseIdentifier, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSMutableDictionary *)allCellReuseIdentifier {
    NSMutableDictionary *dic = objc_getAssociatedObject(self, @selector(allCellReuseIdentifier));
    if (!dic) {
        dic = [NSMutableDictionary dictionary];
        self.allCellReuseIdentifier = dic;
    }
    return dic;
}
@end
