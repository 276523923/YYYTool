//
//  UtilMethod.h
//  BZWDecorationDiary
//
//  Created by yyy on 16/5/18.
//  Copyright © 2016年 yyy. All rights reserved.
//

#ifndef YYYUtilMethod_h
#define YYYUtilMethod_h

#import <UIKit/UIKit.h>
#import "UIDevice+YYYAddition.h"

typedef NS_ENUM(NSUInteger, iPhoneModel) {
    iPhoneModel_3,
    iPhoneModel_4,
    iPhoneModel_5,
    iPhoneModel_6,
    iPhoneModel_6P,
    iPhoneModel_X,
    iPhoneModel_XM,
    iPhoneModel_XR,
};

NS_INLINE iPhoneModel YYYiPhoneModel() {
    static iPhoneModel iPhoneModelValue = iPhoneModel_6;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if ([[UIDevice currentDevice].machineModelName hasPrefix:@"iPhone X"]) {
            NSDictionary *style = @{@"iPhone X" : @(iPhoneModel_X),
                                    @"iPhone XS" : @(iPhoneModel_X),
                                    @"iPhone XS Max" : @(iPhoneModel_XM),
                                    @"iPhone XR" : @(iPhoneModel_XR)};
            NSNumber *model = style[[UIDevice currentDevice].machineModelName];
            if (model) {
                iPhoneModelValue = model.integerValue;
            } else {
                iPhoneModelValue = iPhoneModel_X;
            }
        } else {
            CGSize size = [[UIScreen mainScreen] currentMode].size;
            if (CGSizeEqualToSize(CGSizeMake(320, 480), size)) {
                iPhoneModelValue = iPhoneModel_3;
            } else if (CGSizeEqualToSize(CGSizeMake(640, 960), size)) {
                iPhoneModelValue = iPhoneModel_4;
            } else if (CGSizeEqualToSize(CGSizeMake(640, 1136), size)) {
                iPhoneModelValue = iPhoneModel_5;
            } else if (CGSizeEqualToSize(CGSizeMake(750, 1334), size)) {
                iPhoneModelValue = iPhoneModel_6;
            } else if (CGSizeEqualToSize(CGSizeMake(1242, 2208), size)) {
                iPhoneModelValue = iPhoneModel_6P;
            } else if (CGSizeEqualToSize(CGSizeMake(1125, 2436), size)) {
                iPhoneModelValue = iPhoneModel_X;
            } else if (CGSizeEqualToSize(CGSizeMake(1242, 2688), size)) {
                iPhoneModelValue = iPhoneModel_XM;
            } else if (CGSizeEqualToSize(CGSizeMake(828, 1792), size)) {
                iPhoneModelValue = iPhoneModel_XR;
            } else {
                if ([UIDevice currentDevice].isPad) {
                    iPhoneModelValue = iPhoneModel_6P;
                } else {
                    if (@available(iOS 12, *)) {
                        iPhoneModelValue = iPhoneModel_X;
                    } else {
                        iPhoneModelValue = iPhoneModel_6;
                    }
                }
            }
        }
    });
    return iPhoneModelValue;
}

/**
 *  (320, 480)的屏幕
 */
NS_INLINE BOOL YYYiPhone3() {
    return YYYiPhoneModel() == iPhoneModel_3;
}

/**
 *  (640, 960)的屏幕
 */
NS_INLINE BOOL YYYiPhone4() {
    return YYYiPhoneModel() == iPhoneModel_4;
}

/**
 *  (640, 1136)的屏幕·
 */
NS_INLINE BOOL YYYiPhone5() {
    return YYYiPhoneModel() == iPhoneModel_5;
}

/**
 *  (750, 1334)的屏幕
 */
NS_INLINE BOOL YYYiPhone6() {
    return YYYiPhoneModel() == iPhoneModel_6;
}

/**
 *  (1242, 2208)的屏幕
 */
NS_INLINE BOOL YYYiPhone6P() {
    return YYYiPhoneModel() == iPhoneModel_6P;
}

//iphone6及以上手机
NS_INLINE BOOL YYYiphone6AndUper() {
    return YYYiPhoneModel() >= iPhoneModel_6;
}

NS_INLINE BOOL YYYiPhoneX() {
    return YYYiPhoneModel() == iPhoneModel_X;
}

NS_INLINE BOOL YYYiPhoneXM() {
    return YYYiPhoneModel() == iPhoneModel_XM;
}

NS_INLINE BOOL YYYiPhoneXR() {
    return YYYiPhoneModel() == iPhoneModel_XR;
}

NS_INLINE BOOL YYYiPhoneXStyle() {
    return YYYiPhoneModel() >= iPhoneModel_X;
}

NS_INLINE CGFloat YYYTabbarHeight() {
    return [UIDevice currentDevice].isPadPro3rd? 65 : (YYYiPhoneXStyle() ? 83.f : 49.f);
}

NS_INLINE CGFloat YYYStatushBarHeight() {
    return [UIDevice currentDevice].isPadPro3rd? 24 : (YYYiPhoneXStyle() ? 44.f : 20.f);
}

NS_INLINE CGFloat YYYNavigationBarHeight() {
    return [UIDevice currentDevice].isPadPro3rd? 50 : 44;
}

#ifndef iPhone3
#define iPhone3 YYYiPhone3()
#endif

#ifndef iPhone4
#define iPhone4 YYYiPhone4()
#endif

#ifndef iPhone5
#define iPhone5 YYYiPhone5()
#endif

#ifndef iPhone6
#define iPhone6 YYYiPhone6()
#endif

#ifndef iPhone6P
#define iPhone6P YYYiPhone6P()
#endif

#ifndef iphone6AndUper
#define iphone6AndUper YYYiphone6AndUper()
#endif

#ifndef iPhoneX
#define iPhoneX YYYiPhoneX()
#endif

#ifndef iPhoneXM
#define iPhoneXM YYYiPhoneXM()
#endif

#ifndef iPhoneXR
#define iPhoneXR YYYiPhoneXR()
#endif

#ifndef iPhoneXStyle
#define iPhoneXStyle YYYiPhoneXStyle()
#endif


/**
 判断是否为空对象,NSNull,NSArray count = 0,NSDictionary count = 0,NSString length = 0,都为空
 */
extern BOOL IsEmptyObj(id obj);

extern BOOL NoEmptyObj(id obj);

/**
 判断是否为非空数组，只有对象为数组，且 count > 0 ，才返回YES
 */
NS_INLINE BOOL NotEmptyArray(id array) {
    if ([array isKindOfClass:NSArray.class] && [(NSArray *) array count] > 0) {
        return YES;
    }
    return NO;
}

/**
 如果obj为空，则返回defaultValue
 */
extern id DefaultObj(id obj, id defaultValue);

/**
 对象字符串化，为空则返回 @“”
 */
extern __attribute((overloadable)) NSString *StringObj(id obj);

/**
 对象字符串化，为空对象则返回 defaultString
 */
extern __attribute((overloadable)) NSString *StringObj(id obj, NSString *defaultString);

extern NSString *StringNum(id obj);

/**
 obj 转布尔值
 */
extern BOOL BOOLValue(id obj);

extern void YYYBenchmark(void (^block)(void), void (^complete)(double ms));

__attribute((overloadable)) NS_INLINE CGFloat iPhoneFloatValue(CGFloat iP5, CGFloat iP6, CGFloat iP6P, CGFloat iPX) {
    switch (YYYiPhoneModel()) {
        case iPhoneModel_5:
            return iP5;
        case iPhoneModel_6:
            return iP6;
        case iPhoneModel_6P:
            return iP6P;
        case iPhoneModel_X:
        case iPhoneModel_XM:
        case iPhoneModel_XR:
            return iPX;
        default:
            return iP6;
    }
}

__attribute((overloadable)) NS_INLINE CGFloat iPhoneFloatValue(CGFloat iP5, CGFloat iP6, CGFloat iP6P) {
    return iPhoneFloatValue(iP5, iP6, iP6P, iP6P);
}

__attribute((overloadable)) NS_INLINE CGFloat iPhoneFloatValue(CGFloat iP5, CGFloat iPhone6UpValue) {
    return iPhoneFloatValue(iP5, iPhone6UpValue, iPhone6UpValue);
}

NS_INLINE id iPhoneIDValue(id iP5, id iP6, id iP6P, id iPX) {
    switch (YYYiPhoneModel()) {
        case iPhoneModel_5:
            return iP5;
        case iPhoneModel_6:
            return iP6;
        case iPhoneModel_6P:
            return iP6P;
        case iPhoneModel_X:
        case iPhoneModel_XR:
        case iPhoneModel_XM:
            return iPX;
        default:
            return iP6;
    }
}

NS_INLINE id iPhoneXValue(id iPXAndUper,id other) {
    return YYYiPhoneXStyle() ? iPXAndUper:other;
}

NS_INLINE CGFloat iPhoneXFloatValue(CGFloat iPXAndUper, CGFloat other) {
    return YYYiPhoneXStyle() ? iPXAndUper:other;
}

NS_INLINE UIFont *FONT(CGFloat size) {
    return [UIFont systemFontOfSize:YYYiPhoneModel() >= iPhoneModel_6P ? size + 1 : size];
}

NS_INLINE UIFont *BOLDFONT(CGFloat size) {
    return [UIFont boldSystemFontOfSize:YYYiPhoneModel() >= iPhoneModel_6P ? size + 1 : size];
}

NS_INLINE UIFont *LIGHTFONT(CGFloat size) {
    static BOOL hasLightFont = NO;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        UIFont *font = [UIFont fontWithName:@"PingFangSC-Light" size:10];
        if (font) {
            hasLightFont = YES;
        }
    });
    return hasLightFont ? [UIFont fontWithName:@"PingFangSC-Light" size:YYYiPhoneModel() >= iPhoneModel_6P ? size + 1 : size] : FONT(size);
}

NS_INLINE UIFont *REGULARFONT(CGFloat size) {
    static BOOL hasRegularFont = NO;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        UIFont *font = [UIFont fontWithName:@"PingFangSC-Regular" size:10];
        if (font) {
            hasRegularFont = YES;
        }
    });
    return hasRegularFont ? [UIFont fontWithName:@"PingFangSC-Regular" size:YYYiPhoneModel() >= iPhoneModel_6P ? size + 1 : size] : FONT(size);
}

/**
 Submits a block for asynchronous execution on a main queue and returns immediately.
 */
NS_INLINE void dispatch_async_on_main_queue(void (^block)(void)) {
    dispatch_async(dispatch_get_main_queue(), block);
}

/**
 Submits a block for execution on a main queue and waits until the block completes.
 */
NS_INLINE void dispatch_sync_on_main_queue(void (^block)(void)) {
    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
}

#endif /* UtilMethod_h */
