//
//  YYYBDLocationCoordinate.h
//  Pods
//
//  Created by 叶越悦 on 2018/4/28.
//

#ifndef YYYBDLocationCoordinate_h
#define YYYBDLocationCoordinate_h
#import <CoreLocation/CLLocation.h>

static double x_pi = M_PI * 3000.0 / 180.0;
static inline CLLocationCoordinate2D BDLocationCoordinateToiOSLocationCoordinate(CLLocationCoordinate2D bd) {
    double x = bd.longitude - 0.0065, y = bd.latitude - 0.006;
    double z = sqrt(x * x + y * y) - 0.00002 * sin(y * x_pi);
    double theta = atan2(y, x) - 0.000003 * cos(x * x_pi);
    CLLocationDegrees gg_lon = z * cos(theta);
    CLLocationDegrees gg_lat = z * sin(theta);
    return CLLocationCoordinate2DMake(gg_lat, gg_lon);
}

static inline CLLocationCoordinate2D iOSLocationCoordinateToBDLocationCoordinate(CLLocationCoordinate2D gg) {
    double x = gg.longitude, y = gg.latitude;
    double z = sqrt(x * x + y * y) + 0.00002 * sin(y * x_pi);
    double theta = atan2(y, x) + 0.000003 * cos(x * x_pi);
    CLLocationDegrees bd_lon = z * cos(theta) + 0.0065;
    CLLocationDegrees bd_lat = z * sin(theta) + 0.006;
    return CLLocationCoordinate2DMake(bd_lat, bd_lon);
}


#endif /* YYYBDLocationCoordinate_h */
